						13sep2001	16jul2002
Big picture {{{1

XML base specifications at http://www.w3.org/TR/xmlbase/

XML Schema: 
makes it possible to define the structure and format of "classes" of XML
documents, providing more advanced features than those offered by the regular
Document Type Definition (DTD). (www.w3.org/XML/Schema)

XLink:
specification for linking XML data structures together, in much the same way as
the hyperlinks available in HTML...although XLink allows for far more
sophisticated types of links, including simultaneous links to more than one
resource. (www.w3.org/XML/Linking)

XPointer:
specification for navigating the hierarchical tree structure of an XML
document, and referencing elements, attributes and other data structures within
the document. (www.w3.org/XML/Linking)

XSL and XSLT:
The Extensible Stylesheet Language (XSL) makes it possible to apply presenta-
tion rules to XML documents, and convert - or transform - them from one format
to another. (www.w3.org/Style/XSL/)

XHTML: The next version of HTML, XHTML combines the precision of XML markup
with the easy-to-understand tags of HTML to create a more powerful and flexible
language. (www.w3.org/MarkUp/)

XForms:
offers a way to improve the current crop of HTML-based forms by separating the
function of the form from its appearance, thereby making it possible to easily
adapt a form for display on a variety of devices and systems.
(www.w3.org/MarkUp/Forms/)

XML Query:
effort is focused on creating a specification that makes it possible to query
one or more XML document(s) and generate usable result data (in much the same
way as SQL is used to retrieve database records.)
(www.w3.org/XML/Query)

XML Encryption:
means of encrypting and decrypting XML documents, so as to secure it against
unauthorized usage. (www.w3.org/Encryption/2001/)
}}}1
Simple example: {{{1
<?xml version="1.0"?>
<review>
	<genre>Action</genre>
	<title>X-Men</title>
	<cast>
		<person>Hugh Jackman</person>
		<person>Patrick Stewart</person>
	</cast>
	<director>Bryan Singer</director>
	<year>2000</year>
	<body>Every once in a while, Hollywood takes a comic-book hero, shoots
	him on celluloid, slaps in a few whiz-bang special effects and stands
	back to see the reaction. Sometimes the results are memorable
	(<title>Superman</title>, <title>Spiderman</title>, <title>Flash
	Gordon</title>) and sometimes disastrous (<title>Spawn</title>,
	<title>The Avengers</title>). Luckily, <title>X-Men</title> falls into
	the former category - it's a clever, well-directed film that should
	please both comic-book aficionados and their less well-read cousins.
	</body>
	<rating>4</rating>
</review> }}}1

Document{{{1

Document witten by XML specs is well-formed. The well-formed doc can be valid
if it meets certain further constraints.
Each XML document has both a logical and a physical structure. Physically,
the document is composed of units called entities. An entity may refer to other
entities to cause their inclusion in the document. Logically, the document is
composed of declarations, elements, comments, character references, and pro-
cessing instructions, all of which are indicated in the document by explicit
markup. The logical and physical structures must nest properly.

}}}1
Well-formed doc {{{1
	1.Taken as a whole, it matches the production labeled document.
	2.It meets all the well-formedness constraints given in this spec.
	3.Each of the parsed entities which is referenced directly or in-
	  directly within the document is well-formed.
	    
}}}1

element		::=	EmptyElemTag
			| STag content ETag
EmptyElemTag	::=	'<' Name (S Attribute)* S? '/>'
	examples
	<IMG align="left" src="http://www.w3.org/Icons/WWW/w3c_home" />
	<br></br>
	<br/>
STag		::=	'<' Name (S Attribute)* S? '>'
	example
	<termdef id="dt-dog" term="dog">
ETag		::=	'</' Name S? '>'
	example
	</termdef>
content		::=	CharData? ((element | Reference | CDSect | PI
			| Comment) CharData?)* /* */

 canonical XML document conforms to the following grammar: 
 CanonXML    ::= Pi* element Pi*
 element     ::= Stag (Datachar | Pi | element)* Etag
 Stag        ::= '<'  Name Atts '>'
 Etag        ::= '</' Name '>'
 Pi          ::= '<?' Name ' ' (((Char - S) Char*)? - (Char* '?>' Char*)) '?>'
 Atts        ::= (' ' Name '=' '"' Datachar* '"')*
 Datachar    ::= '&amp;' | '&lt;' | '&gt;' | '&quot;'
                  | '&#9;'| '&#10;'| '&#13;'
		  | (Char - ('&' | '<' | '>' | '"' | #x9 | #xA | #xD))
 Name        ::= (see XML spec)
 Char        ::= (see XML spec)
 S           ::= (see XML spec)
				   
