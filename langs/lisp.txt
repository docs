List is sequence of words enclosed between parenthesis like ( + 2 2 abs )
Evaluate list: write in Lisp mode of Xemacs following (+ 2 2) , place cursor right after and press C-x C-e. Will get result 4 in echo area. Echo area show values returned by expressions.
Try the same w/ qouted list ( like �(abd cdefg) ) and will get in echo area the same �(abd cdefg) cause quote before list tells to Lisp interpreter to do nothing.
Try to evaluate list (this is unquoted list) and will get
�Symbol�s function definition is void: this�
Evaluating inner list: (+ 2 (+ 3 3)) will give 8.
Use ; for remarks. The keypress C-x C-e actually call eval-last-sexp func.
Symbol can have func attach to it or value attach to it ( it would be variable).
If  try to evaluate fill-column will get 70 or 72. But if try to evaluate
(fill-column) will get error �Symbol�s function definition is void: this�
It�s mean in list where first word fill-column that is not connected to any func.
Error message when evaluate symbol w/o value. In sequence (+ 2 2) put cursor after +
and C-x C-e. Will get error �Symbol�s value as variable is void: +�
Cause cursor was after the + Lisp interpreter take expression w/o parenthesis so looks at + like
variable ( + have not value so error ).
Concatenating 2 strings: (concat �abc� �def�) will give �abcdef�
Substringing: (substring �abc def gh� 4 7) will give �def�
Arguments as values of var: (concat �This is � (+ 2 fill-column) � things.�) will give 
�This is 72 things�
Expression (+ 2 3 4) evaluates to 9
Func message (message �abc�) will see in echo area �abc� cause echo area shows return of func.
(message �The name of buffer is: %s� (buffer-name))
(message �The value of fill-column is %d� fill-column)
(message �there are %d %s in the office!� (- fill-column 20) �pink mouses�) will show 
�there are 50 pink mouses in the office!�
Many ways to bind var to value.Using funcs set or setq, using let.
Using set.
(set �flowers �(pink red white)) will bind flowers to list (pink red white) so if evaluate var flowers it will show list.
Have to put quote before args of set in order not to evaluate each of them just to send to set.
The combination of set and quote of first arg so common that it has it�s own name setq.
Also setq anm be used to assign diff values to diff vars.
(setq simpl �(abc def gh) nsimpl �(sdf asd))
Counting.
(setq counter 0)
(setq counter (+ counter 1))
counter
If evaluate first and third expression will have 0, and after second and third will have 1,so keep
doing second and third and see incrementation. Lisp first evaluates inner list so counter get cuurent value + 1.

