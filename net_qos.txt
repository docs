																	31mar02
										Abbreviations:
								packet(s) - pkt(s)
								interrupt(s) - intrp(s)
								Fast Forwarding - FF
By kernel 2.2.x network subsystem not threaded and only 1 pkt at a time could
enter system.
Introduction (2.3.43) of softnet patch that creates backlog queue per processor,
meant network stack can concurrently process as many pkts as processors.
>From 2.3.58 IRQ affinity introduced.In SMP machine set of processors can be de-
dicate to do network processing,by attaching interfaces to the set,while
other processors for other types of workloads.
On 2.3.99 was study called Fast Forwarding found that Linux reaches con-
gestion collapse at 60Kpps(K pkts per sec).

source: http://lwn.net/2001/features/KernelSummit/ (march 2001)
	The network driver API (Kernel summit)
Jamal Hadi Salim led a session describing changes to the network driver
interface. The stock Linux kernel performs poorly under very heavy network
loads- as the number of pkts received goes up, the number of pkts 
actually processed begins to drop, until it approaches zero in especially 
hostile situations. The desire, of course, is to change that.A number of 
problems have been indentified in the current networking stack, including:
* In heavy load situations, the too many intrps are generated. When several
  tens of thousands of pkts must be dispatched every second, the system sim-
  ply does not have the resources to deal with a hardware intrp for every pkt.
* When load gets heavy, the system needs to start dropping pkts. Currently, 
  pkts are dropped far too late, after resources have been expended on them. 
* Pkts are examined and classified several times;to do once and remember.
* On SMP systems, pkts can be reordered in the networking subsystem, leading to
  suboptimal performance later on. 
* Heavy load on a single interface can lead to unfair behavior as the other 
  interfaces on the system are ignored. 
Jamal's work (done with Robert Olsson and Alexey Kuznetsov) has focused 
primarily on the first two problems.The 1st thing that has been done is to 
provide a mechanism to tell drivers that the networking load is high. The 
drivers should then tell their interfaces to cut back on intrps. After all,when 
a heavy stream of pkts is coming in, there will always be a few of them waiting 
in the DMA buffers, and the intrps carry little new information.When intrps are 
off, the networking code will instead poll drivers when it is ready to accept 
new pkts. Each interface has a quota stating how many pkts will be accepted; 
this quota limits the pkt traffic into the rest of the kernel, and distributes 
processing more fairly across the interfaces. If the traffic is heavy, it is 
entirely likely that the DMA rings for one or more drivers will overflow, since 
the kernel is not polling often enough. Once that happens, pkts will be dropped 
by the interface itself (it has, after all, no place to put them). Thus the 
kernel need not process them at all, and they do not even cross the I/O bus.
The end result is an order of magnitude increase in the number of pkts a Linux 
system can route. This work is clearly successful, and will likely show up in 
2.5 in some form.

source: http://lwn.net/2001/1004/kernel.php3 (october 2001)
The NAPI work is based on the techniques discussed before, but the work has 
progressed since then. It has not, perhaps, received the degree of attention 
that it should have, though this discussion has raised its profile somewhat.
Now it might become truly widely known...
NAPI works with modern network adaptors which implement a "ring" of DMA buffers;
each pkt, as it is received,is placed into the next buffer in the ring.Normally,
the processor is intrped for each packet,and the system is expected to empty the
packet from the ring. The NAPI patch responds to the 1st intrp by telling the
adaptor to stop intrping;it will then check the ring occasionally as it
processes pkts and pull new ones without the need for further intrps.
People who have been on the net for a long time might appreciate this analogy:
back in the 1980's, many of us had our systems configured to beep (intrp) at us 
ever time an email message arrived. In 2001, beeping mail notifiers are far less
common. There's almost always new mail, there's no need for the system to be 
obnoxious about it. Similarly, on a loaded system, there will always be new 
pkts to process, so there is no need for all those intrps.
When the networking code checks an interface and finds that no more pkts have
arrived, intrps are reenabled and polling stops.
NAPI takes things little farther by eliminating the pkt backlog queue currently 
maintained in the 2.4 network stack. Instead, the adaptor's DMA ring becomes 
that queue. In this way, system memory is conserved, pkts are less likely to be 
reordered, and, if the load requires that pkts be dropped, they will be disposed
of before ever being copied into the kernel.
NAPI requires some changes to the network driver interface,of course.The changes
have been designed to be incremental. Drivers which have not been converted will
continue to function as always (well, at least, as in 2.4.x), but the higher 
performance enabled by NAPI will require modifications.

