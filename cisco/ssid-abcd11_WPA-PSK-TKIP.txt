Cisco
Cisco
en
Cisco
configure terminal
no dot11 ssid abcd11
interface dot11radio 0
no encryption key 1
no encryption key 2
no encryption key 3
no encryption key 4
encryption mode ciphers tkip
ssid abcd11
guest-mode
no authentication shared
no authentication network-eap
no authentication key-management
no authentication client
no wpa-psk
authentication open
authentication key-management wpa
wpa-psk ascii 12345678
end
configure terminal
interface dot11radio 1
no encryption key 1
no encryption key 2
no encryption key 3
no encryption key 4
encryption mode ciphers tkip
ssid abcd11
guest-mode
no authentication shared
no authentication network-eap
no authentication key-management
no authentication client
no wpa-psk
authentication open
authentication key-management wpa
wpa-psk ascii 12345678
end
write memory
quit
