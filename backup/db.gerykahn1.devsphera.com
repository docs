;
; @@DOMAIN@@ zone file
;
$TTL 10800
@ IN SOA gerykahn.devsphera.com. admin.gerykahn.devsphera.com. ( 2 10800 3600 604800 86400 )
;
; Nameservers here
;
 IN NS gerykahn.devsphera.com.
;
; <address> is the IP address of the VDS.
;
 IN A 172.16.1.152
 IN MX 10 mailserverlocation
;
; Host aliases here
;
mailserverlocation IN A 172.16.1.152
www IN CNAME @ ;1
mail IN CNAME @ ;2
smtp IN CNAME @ ;3
pop IN CNAME @ ;4
ftp IN CNAME @ ;5
