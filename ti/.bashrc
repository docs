# .bashrc

# User specific aliases and functions

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi
EDITOR=vim
set -o vi
OS=`uname -s`


function getXserver()
{
	case $TERM in
		'xterm')
			XSERVER=$(who am i | awk '{print $NF}' | tr -d ')''(' )
			XSERVER=${XSERVER%%:*}
		;;
		'aterm')
		;;
	esac
}

if [ -z ${DISPLAY:=''} ]; then
	getXserver
	if [[ -z ${XSERVER} || ${XSERVER} == $(hostname) || \
		${XSERVER} == "unix" ]]; then
		DISPLAY=":0.0"
	else
		DISPLAY=${XSERVER}:0.0
	fi
fi

export DISPLAY

case "$OS" in
    'Linux')
	PATH="/bin:/sbin:/usr/bin:/usr/sbin:/usr/bin/X11:/usr/X11R6/bin"
	PATH="/usr/local/bin:/usr/local/sbin:${PATH}"
	case "$HOSTNAME" in
	'ubtdsk2') # TI's linux
		#PATH="/usr/sbin:/usr/bin:/sbin:/bin/:/usr/local/sbin:/usr/local/bin:/usr/bin/X11:/usr/X11R6/bin:/work/CodeSourcery/Sourcery_G++_Lite/bin"
		PATH="/usr/sbin:/usr/bin:/sbin:/bin/:/usr/local/sbin:/usr/local/bin:/usr/bin/X11:/usr/X11R6/bin:/work/CodeSourcery/arm-2010q1/bin"
		#alias zoom2='export ARCH=arm && export CROSS_COMPILE=arm-none-linux-gnueabi-\
			# && export NFSROOT=/work/zoom2/nfsroot'
		PATH="/work/ti/chromium/depot_tools:${PATH}"
		source ~/.zoom2rc
		alias p1271='export NFSROOT=/work/zoom2/1271_nfs'
		alias p1281e='export NFSROOT=/work/zoom2/nfsroot'
		alias p1273e='export NFSROOT=/work/zoom2/1273e'
		alias p1273='export NFSROOT=/work/zoom2/1273'
		alias pbb='export NFSROOT=/work/bb/1273'
		alias pbba='export NFSROOT=/work/bb/Angstrom'
		#alias arm1='export PATH="/work/CodeSourcery/arm-2010q1/bin/:$PATH"'
	;;
	'ub102666') # 32-bit
		PATH="/usr/sbin:/usr/bin:/sbin:/bin/:/usr/local/sbin:/usr/local/bin:/usr/bin/X11:/usr/X11R6/bin:/opt/CodeSourcery/arm-2010q1/bin"
		source ~/.zoom2rc
		alias pz3e='export NFSROOT=/opt/rootfs/z3e'
		alias pz3e3='export NFSROOT=/opt/rootfs/z3e3'
		alias p1273='export NFSROOT=/opt/rootfs/z3'
		alias pbb='export NFSROOT=/opt/rootfs/beagle'
		alias ppanda='export NFSROOT=/opt/rootfs/panda'
		alias pblaze='export NFSROOT=/opt/rootfs/blaze'
		alias parm='export PATH="/opt/CodeSourcery/arm-2010.09/bin/:$PATH"'
		#export http_proxy="http://wwweuproxy.itg.ti.com:80/"
		#export https_proxy="http://wwweuproxy.itg.ti.com:80/"
		#export ftp_proxy="http://wwweuproxy.itg.ti.com:80/"
	;;
	'ub102667') # 64-bit
		PATH="/usr/sbin:/usr/bin:/sbin:/bin/:/usr/local/sbin:/usr/local/bin:/usr/bin/X11:/usr/X11R6/bin:/opt/CodeSourcery/arm-2010q1/bin"
		source ~/.zoom2rc
		alias p1273e='export NFSROOT=/opt/rootfs/z3e'
		alias p1273='export NFSROOT=/opt/rootfs/z3'
		alias pbb='export NFSROOT=/opt/rootfs/bb'
		alias pbba='export NFSROOT=/opt/rootfs/bbA'
		alias ppanda='export NFSROOT=/opt/rootfs/panda'
		alias parm='export PATH="/opt/CodeSourcery/arm-2010.09/bin/:$PATH"'
	;;
        'nikkei')
                eval "`dircolors -b ~/dircolors`"
            	export IRP_SRC=~/scopus
                export IRP_PLATFORM=irp
                export IRP_BUILD_ARCH=ppc
                alias bml='export IRP_PLATFORM=ml && source $IRP_SRC/irp/linux-add-ons/scripts/env-pro-ppc-405'
                alias birpd8='export IRP_PLATFORM=irp && export IRP_NO_DEBUG= && export IRP_STANDALONE= && export IRP_8INPUTS=1 && source $IRP_SRC/irp/linux-add-ons/scripts/env-pro-ppc-405'
                alias birpd='export IRP_PLATFORM=irp && export IRP_NO_DEBUG= && export IRP_STANDALONE= && export IRP_8INPUTS= && source $IRP_SRC/irp/linux-add-ons/scripts/env-pro-ppc-405'
                alias birp='export IRP_PLATFORM=irp && export IRP_NO_DEBUG=1 && export IRP_STANDALONE= && source $IRP_SRC/irp/linux-add-ons/scripts/env-pro-ppc-405'
                alias birps='export IRP_PLATFORM=irp && export IRP_NO_DEBUG= && export IRP_STANDALONE=1 && source $IRP_SRC/irp/linux-add-ons/scripts/env-pro-ppc-405'
                alias irpp='echo $IRP_PLATFORM'
				export P4USER=geryk
				export P4CLIENT=ForGery
                export USE_NEWWAY=1
                export MINICOM="-m -c on"
                export ANT_HOME="/opt/apache-ant-1.7.0"
                export JAVA_HOME="/opt/jdk1.6.0_07"
				PATH="/home/gxk/prjs/scripts/libsh:/usr/local/p4:/usr/local/firefox:/opt/apache-ant-1.7.0/bin:\
/home/gxk/prjs/scripts:/home/gxk/prjs/pBuilder:${PATH}"
				#alias xil9='source /opt/Xilinx91i/settings.sh && source /opt/EDK/settings.sh && source /opt/chipscope/settings.sh'
				#alias xil10='source /opt/Xilinx10.1/ISE/settings32.sh && source /opt/Xilinx10.1/EDK/settings32.sh'
	    	;;
	    	'gery-lnx')
                PATH="/opt/mv_pro_5.0/montavista/pro/devkit/arm/v5t_le/bin:/opt/mv_pro_5.0/montavista/pro/bin:/opt/mv_pro_5.0/montavista/common/bin:$PATH"
	    	;;
	    	'smiley.scopus.net')
            	export IRP_SRC=~/scopus
                export IRP_PLATFORM=irp
                export IRP_BUILD_ARCH=ppc
                alias bml='export IRP_PLATFORM=ml && source $IRP_SRC/irp/linux-add-ons/scripts/env-pro-ppc-405'
                alias birpd8='export IRP_PLATFORM=irp && export IRP_NO_DEBUG= && export IRP_STANDALONE= && export IRP_8INPUTS=1 && source $IRP_SRC/irp/linux-add-ons/scripts/env-pro-ppc-405'
                alias birpd='export IRP_PLATFORM=irp && export IRP_NO_DEBUG= && export IRP_STANDALONE= && export IRP_8INPUTS= && source $IRP_SRC/irp/linux-add-ons/scripts/env-pro-ppc-405'
                alias birp='export IRP_PLATFORM=irp && export IRP_NO_DEBUG=1 && export IRP_STANDALONE= && source $IRP_SRC/irp/linux-add-ons/scripts/env-pro-ppc-405'
                alias birps='export IRP_PLATFORM=irp && export IRP_NO_DEBUG= && export IRP_STANDALONE=1 && source $IRP_SRC/irp/linux-add-ons/scripts/env-pro-ppc-405'
                alias irpp='echo $IRP_PLATFORM'
				export P4USER=geryk
				export P4CLIENT=ForGery
                export USE_NEWWAY=1
                export MINICOM="-m -c on"
                export ANT_HOME="/opt/apache-ant-1.7.0"
                export JAVA_HOME="/opt/jdk1.6.0_07"
				PATH="/home/gxk/prjs/scripts/libsh:/usr/local/p4:/usr/local/firefox:/opt/apache-ant-1.7.0/bin:\
/home/gxk/prjs/scripts:/home/gxk/prjs/pBuilder:${PATH}"
				alias xil9='source /opt/Xilinx91i/settings.sh && source /opt/EDK/settings.sh && source /opt/chipscope/settings.sh'
				alias xil10='source /opt/Xilinx10.1/ISE/settings32.sh && source /opt/Xilinx10.1/EDK/settings32.sh'
	    	;;
	    	'bashful.scopus.net')
				PATH="/home/gxk/prjs/scripts/libsh:/usr/local/p4:/home/gxk/prjs/scripts:${PATH}"
            	export IRP_SRC=~/scopus
				export P4USER=geryk
				export P4CLIENT=forgery
	    	;;
	    	'gxk-dt-lnx.softier1.local')
				PATH="/usr/local/mozilla:${PATH}"

				export CVSROOT=":pserver:gxk@linux-s:/cvs"

				alias tmsgcc='tms320c6-coff-gcc'
				alias tmsld='tms320c6-coff-ld'
				alias tmsar='tms320c6-coff-ar'
				alias tmsnm='tms320c6-coff-nm'
	    	;;
	    	*) echo missing HOSTNAME.
	    	;;
		esac
		export PATH Path EDITOR
    ;;
    'CYGWIN_NT-5.1')
		PATH=".:/bin:/sbin:/usr/bin:/usr/sbin:/usr/bin/X11:/usr/X11R6/bin"
		PATH="/usr/local/bin:/usr/local/sbin:${PATH}"
    ;;
    *) echo missing OS name. check "uname".
    ;;
esac

alias difff='diff -pbBwuN'

# FIND
alias findf='find `pwd` -type f -follow'
alias findd='find `pwd` -type d -follow'
alias findg="find `pwd` -type f -follow | xargs grep \!*"
alias findch="find `pwd` -type f -name '*.[c|h]'"
alias findchg="find `pwd` -type f -name '*.[c|h]' | xargs grep \!*"
#GREP
alias g='grep'
alias gk='grep -r --exclude-dir=.git --exclude={tags,*~,*.swp,*.o,*.ko,cscope.out}'
alias gkl='grep -rl --exclude-dir=.git --exclude={tags,*~,*.swp,*.o,*.ko,cscope.out}'
#GIT
alias gb='git branch'
alias gba='git branch -a'
alias gc='git commit -v'
alias gd='git diff'
alias gl='git pull'
alias gp='git push'
alias gst='git status'

alias j='jobs'
alias l='less'
alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'
alias ls='ls --color=auto'
alias ll='ls -l'
alias lla='ls -la'

# screen sessions
alias mscr='screen -U -S main'
# terminals
alias scrix='xterm -geometry 140x75 -e "screen -U -S main"'
alias work='xterm -geometry 209x75+0+0 -e "screen -U -S main"'
alias vix='xterm -b 1 -ms red -geom 101x54 -e ${EDITOR}'
alias tsb3='xterm -T root -b 1 -bg SkyBlue3 -fg black -ms red -geom 80x45 -e sudo bash&'
alias mail='xterm -bg black -fg white -geom 90x70 -e mutt&'
alias tg20='xterm -bg grey20 -fg white&'
alias tsb1='xterm -bg SkyBlue1 -fg black&'
alias tlsb1='xterm -bg LightSteelBlue1 -fg black&'

#alias win='rdesktop -u geryk -d ivirtus -g 95% 192.168.1.36 &'
alias win='rdesktop -u a0387671 -d ENT -g 95% 137.167.20.87 &'
