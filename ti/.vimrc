if v:lang =~ "utf8$" || v:lang =~ "UTF-8$"
   set fileencodings=utf-8,latin1
endif

set nocompatible	" Use Vim defaults (much better!)
set bs=indent,eol,start		" allow backspacing over everything in insert mode
set ai			" always set autoindenting on
set backup		" keep a backup file
set viminfo='20,\"50	" read/write a .viminfo file, don't store more
			" than 50 lines of registers
set history=100		" keep 50 lines of command line history
set ruler		" show the cursor position all the time

" Only do this part when compiled with support for autocommands
if has("autocmd")
  " In text files, always limit the width of text to 78 characters
  autocmd BufRead *.txt set tw=78
  " When editing a file, always jump to the last cursor position
  autocmd BufReadPost *
  \ if line("'\"") > 0 && line ("'\"") <= line("$") |
  \   exe "normal! g'\"" |
  \ endif
endif

if has("cscope") && filereadable("/usr/bin/cscope")
   set csprg=/usr/bin/cscope
   set csto=0
   set cst
   set nocsverb
   " add any database in current directory
   if filereadable("cscope.out")
      cs add cscope.out
   " else add database pointed to by environment
   elseif $CSCOPE_DB != ""
      cs add $CSCOPE_DB
   endif
   set csverb
endif

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
endif

if &term=="xterm"
     set t_Co=8
     set t_Sb=[4%dm
     set t_Sf=[3%dm
endif

" gxk 06jun2007
set shell=bash
set showmode
set textwidth=80
set helpheight=50
set filetype=on

" kernel settings
set tabstop=8 softtabstop=8 shiftwidth=8 noexpandtab
" previous settings
" set ts=4 expandtab shiftwidth=4

"set mouse=a
set paste
set laststatus=2
if has('statusline')
    " Status line detail:
    " %f                file path
    " %y                file type between braces (if defined)
    " %([%R%M]%)        read-only, modified and modifiable flags between braces
    " %{'!'[&ff=='default_file_format']}
    "                   shows a '!' if the file format is not the platform
    "                   default
    " %{'$'[!&list]}    shows a '*' if in list mode
    " %{'~'[&pm=='']}   shows a '~' if in patchmode
    " (%{synIDattr(synID(line('.'),col('.'),0),'name')})
    "                   only for debug : display the current syntax item name
    " %=                right-align following items
    " #%n               buffer number
    " %l/%L,%c%V        line number, total number of lines, and column number
    function SetStatusLineStyle()
        if &stl == '' || &stl =~ 'synID'
            let &stl="%f %y%([%R%M]%)%{'!'[&ff=='".&ff."']}%{'$'[!&list]}%{'~'[&pm=='']}%=#%n %l/%L,%c%V "
        else
            let &stl="%f %y%([%R%M]%)%{'!'[&ff=='".&ff."']}%{'$'[!&list]} (%{synIDattr(synID(line('.'),col('.'),0),'name')})%=#%n %l/%L,%c%V "
        endif
    endfunc
    " Switch between the normal and vim-debug modes in the status line
    nmap _ds :call SetStatusLineStyle()<CR>
    call SetStatusLineStyle()
    " Window title
    if has('title')
        set titlestring=%t%(\ [%R%M]%)
    endif
endif

" cscope mappings
map g<C-]> :cs find 3 <C-R>=expand("<cword>")<CR><CR>
map g<C-\> :cs find 0 <C-R>=expand("<cword>")<CR><CR>


" folding
" all work done in file .vim/ftdetect/txt.vim
set foldlevel=1
set foldmethod=syntax
highlight Folded ctermbg=lightgray ctermfg=black

" tabbing
set tabpagemax=7
set showtabline=2
" hi TabLineSel ctermfg=darkred ctermbg=gray
"hi TabLine guibg=#465c86 guifg=fg gui=underline
"hi TabLineFill guibg=#4d8080 guifg=fg gui=underline

" taglist
nnoremap <silent> <F8> :TlistToggle<CR>
let g:Tlist_Auto_Update = 1
let g:Tlist_Show_One_File = 1
let g:Tlist_Exit_OnlyWindow = 1

nmap ,t :!(cd %:p:h;ctags *.[ch])&

set isfname-=? isfname+=?
set isfname-=& isfname+=&
let g:Browser_console = 'lynx'
"let g:Browser_console = InScreen('w3m')
nnoremap <silent> gb :exe('!'.g:Browser_console.' "'.expand('<cfile>').'"')<CR>

" set ProggyClean font
"set guifont=ProggyCleanTT\ 12
