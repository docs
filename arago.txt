mkdir /opt/arago-armv5te
pushd /opt/arago-armv5te
tar jxf arago-2011.05-armv5te-linux-gnueabi-toolchain.tar.bz2
export PATH=/opt/arago-armv5te/usr/local/arago/bin:${PATH}

arm-arago-linux-gnueabi-gcc

sudo apt-get install chrpath

mkdir ~/oe
pushd ~/oe
git clone git://arago-project.org/git/arago.git
git clone git://arago-project.org/git/arago-oe-dev.git
git clone git://arago-project.org/git/arago-bitbake.git
cp arago/setenv.sample arago/setenv
cp arago/conf/local.conf.sample arago/conf/local.conf
export OEBASE=$HOME/oe
export SCRATCH=/sim/scratch_AID   ?????
 . arago/setenv

MACHINE=am180x-evm bitbake virtual/kernel
MACHINE=am180x-evm bitbake arago-base-tisdk-image
MACHINE=am180x-evm –c clean bitbake virtual/kernel
