au BufRead,BufNewFile *.txt  setlocal fdm=marker foldlevel=0 fcs=fold:.
au BufRead,BufNewFile *.xml  setlocal fdm=indent foldlevel=0 fcs=fold:.
